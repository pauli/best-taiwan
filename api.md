# 最台湾旅游体验师系统 API



## 参数说明

- jsonp 参数：```cb=callbackXXX```, 返回字符串： ```callbackXXX({...})```
- status: 100-没有登录，202-正常，404-数据不存在
- TOKEN: 是登录用户的凭证



## 登录验证

http://zuitaiwan.net/b2b/apis/login?username=pauli&password=123456

- URL: /api/login
- 请求方式: get/post/jsonp
- 参数: 

```
{
	username: 'YUNI',		//帐号
	password: '123456',		//密码
	secode: '12345',		//验证码
}
```

- 返回: 

```
{
	status: 202,
	token: TOKEN
}
```



## 取得当前用户资料

http://zuitaiwan.net/b2b/apis/get_profile?token=CIJNLAlI3145VeL7b52d7A%3D%3D

- 验证: 当前用户是否登录
- URL: /api/profile/get
- 请求方式: get/jsonp
- 参数: 

```
{
	token: TOKEN
}
```

- 返回: 当前用户资料: 

```
{
	status: 202,
	data: {
		userid: 'T0000001',		//会员编号
		username: 'YUNI',		//帐号
		email: 'lml@qq.com',		//邮箱
		displayname: '米拉',		//姓名
		address: '杭州市萧山区金城路1号',		//地址
		phone: '1377777777',		//电话
		company: '米辰集团',		//公司名称
		corpid: 'michen',		//公司编号
		contact: '小悦',		//联络人
		contactphone: '13888888888',		//联络人电话
		joindate: '2015-05-04 12:13:14',		//加入时间, 字符串, 2015-10-01 12:13:14
	}
}
```



## 修改当前用户资料

http://zuitaiwan.net/b2b/apis/edit_profile?token=CIJNLAlI3145VeL7b52d7A%3D%3D&X1=Y1.....

- 验证: 当前用户是否登录
- URL: /api/profile/edit
- 请求方式: get/post/jsonp
- 参数: 

```
{
	token: TOKEN,
	password: '123456',		//密码（可为空，不修改）
	password2: '123456',		//密码确认
	email: 'lml@qq.com',		//邮箱
	displayname: '米拉',		//姓名
	address: '杭州市萧山区金城路1号',		//地址
	phone: '1377777777',		//电话
	company: '米辰集团',		//公司名称
	corpid: 'michen',		//公司编号
	contact: '小悦',		//联络人
	contactphone: '13888888888',		//联络人电话
}
```

- 返回: 

```
{
	token: TOKEN,
	status: 202,
}
```



## 获得验证码

- URL: /api/secode
- 请求方式: get
- 参数: 为空
- 返回: 图片格式



## 课程设置

http://zuitaiwan.net/b2b/apis/course_get?token=CIJNLAlI3145VeL7b52d7A%3D%3D&startdate=20151114

- 验证: 当前用户是否登录
- URL: /api/course/get
- 请求方式: get/jsonp
- 参数: 

```
{
	token: TOKEN,
	startdate: '2015-05-04'
}
```

- 返回: 

```
{
	status: 202,
	data: {
		startdate: '2015-05-04',		//上课时间
		courseid: 'ABCD',				//课程编号
		coursename: '水上运动体验课',	//课程名称
		totlenum: 30,					//上课人数
		joinnum: 28,					//已报道人数
		joined: 1,						//1-已报道，0-未报道
	}
}
```



## 课程报名信息

http://zuitaiwan.net/b2b/apis/course_joined?token=CIJNLAlI3145VeL7b52d7A%3D%3D&courseid=C201511110001

- 验证: 当前用户是否登录
- URL: /api/course/joined
- 请求方式: get/jsonp
- 参数: 

```
{
	token: TOKEN,
	courseid: 'C201511110001'
}
```

- 返回: 

```
{
	status: 202,
	data: [
		{
			displayname: '张三',	//姓名
			joined: 1,				//1-已报道，0-未报道
		},
		{
			displayname: '李四',	//姓名
			joined: 1,				//1-已报道，0-未报道
		}
	]
}
```


## 扫码报道

http://zuitaiwan.net/b2b/apis/course_qrcode?token=CIJNLAlI3145VeL7b52d7A%3D%3D&courseid=C201511110001&qrcode=ABCD

- 验证: 当前用户是否登录
- URL: /api/course/qrcode
- 请求方式: get/post/jsonp
- 参数: 

```
{
	token: TOKEN,
	courseid: 'C201511110001',
	qrcode: 'ABCD'
}
```

- 返回: 

```
{
	status: 202
}
```
