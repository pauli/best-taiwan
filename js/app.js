angular.module('BestTaiwan', ['ngRoute', 'ngCookies'], function(){
}).run(function(dataService){
	dataService.check();
}).config(function($routeProvider){
	$routeProvider.when('/login', {
		templateUrl: 'loginTmpl',
		controller: 'loginCtrl'
	}).when('/profile', {
		templateUrl: 'profileTmpl',
		controller: 'profileCtrl'
	}).when('/course', {
		templateUrl: 'courseTmpl',
		controller: 'courseCtrl'
	}).when('/logout', {
		templateUrl: 'loginTmpl',
		controller: 'logoutCtrl'
	}).otherwise({
		redirectTo: '/login'
	});
})
.controller('loginCtrl', function($scope, $rootScope, dataService){
	dataService.check();
	$scope.loginSubmit = function(){
		if(!$scope.username || !$scope.password){
			return;
		}
		dataService.get('http://zuitaiwan.net/b2b/apis/login', {
			username: $scope.username,
			password: $scope.password
		}, function(result){
			if(result){
				if(result.status == 202){
					dataService.check(result.token);
				}else{
					$rootScope.consoleMsg = '登录失败：' + result.status;
				}
			}
		});
		return false;
	};
})
.controller('logoutCtrl', function($cookieStore, $location){
	$cookieStore.remove('token');
	$location.path('/login');
})
.controller('profileCtrl', function($scope, $rootScope, $location, dataService){
	if(dataService.check(false) || $rootScope.profile)return;
	dataService.get('http://zuitaiwan.net/b2b/apis/get_profile', {}, function(result){
		if(result){
			if(result.status == 202){
				var joindate = result.data.joindate;
				if(joindate) {
					$scope.joindate = joindate.substring(0, joindate.indexOf(' '));
				}
				$rootScope.profile = result.data;
			}else{
				$rootScope.consoleMsg = '登录失败：' + result.status;
			}
		}
	});
	
	$scope.saveProfile = function(){
		dataService.get('http://zuitaiwan.net/b2b/apis/edit_profile', $rootScope.profile, function(result){
			if(result){
				if(result.status == 202){
					alert('修改资料成功');
					$rootScope.consoleMsg = '修改资料成功';
				}else{
					$rootScope.consoleMsg = '修改失败：' + result.status;
				}
			}
		});
	};
})
.controller('courseCtrl', function($scope, $rootScope, $location, dataService){
	if(dataService.check(false))return;
	var date = new Date();
	$scope.data = {};
	$scope.data.today = dataService.format(date, 0);
	$scope.dateList = [];
	for(var i=0; i<20; i++){	// 前十天，后十天
		$scope.dateList[i] = dataService.format(date, i - 10);
	}
	$scope.joined = function(course){
		if(course.showJoined){
			course.showJoined = false;
			return;
		}
		course.showJoined = true;
		if(course.joinedList) {
			return;
		}
		if(course.totlenum < 1){
			course.joinedList = [];
			return;
		}
		dataService.get('http://zuitaiwan.net/b2b/apis/course_joined', {
			courseid: course.courseid
		}, function(result){
			if(result){
				if(result.status == 202){
					course.joinedList = result.data;
				}else{
					$rootScope.consoleMsg = '加载课程失败：' + result.status;
				}
			}
		});
	};
	$scope.scanQR = function(courseid){
		dataService.appId(function(){
			wx.scanQRCode({
				needResult: 1,
				scanType: ['qrCode', 'barCode'],
				success: function (res) {
					$rootScope.consoleMsg = '扫码: ' + res.resultStr;
					dataService.get('http://zuitaiwan.net/b2b/apis/course_qrcode', {
						courseid: courseid,
						qrcode: res.resultStr,
					}, function(result){
						if(result){
							if(result.status == 202){
								$rootScope.consoleMsg = '扫描成功！';
							}else{
								$rootScope.consoleMsg = '扫描失败：' + result.status;
							}
						}
					});
				}
			});
		});
		return false;
	};
	$scope.getCourse = function(){
		dataService.get('http://zuitaiwan.net/b2b/apis/course_get', {
			startdate: $scope.data.today
		}, function(result){
			if(result){
				if(result.status == 202){
					$scope.courses = result.data;
					$scope.joinedList = [];
					$scope.showJoined = false;
				}else{
					$rootScope.consoleMsg = '加载课程失败：' + result.status;
				}
			}
		})
	};
	$scope.getCourse();
})
.factory('dataService', function($rootScope, $http, $cookieStore, $location){
	return {
		get: function(url, data, callback){
			url += url.indexOf('?') > 0 ? '&' : '?';
			angular.forEach(data, function(val, key){
				url += key + '=' + val + '&';
			});
			url += 'cb=JSON_CALLBACK';
			var token = $cookieStore.get('token');
			if(token){
				url += '&token=' + encodeURIComponent(token);
			}
			$http.jsonp(url).success(callback || angular.noop);
		}, check: function(newToken){
			$rootScope.consoleMsg = '';
			var status = false, token = $rootScope.token = newToken || $cookieStore.get('token');
			if(!token){
				$location.path('/login');
				return true;
			}else if(newToken !== false){
				$location.path('/course');
				status = true;
			}
			//$rootScope.consoleMsg = 'token: ' + token;	//TODO
			$cookieStore.put('token', token);
			return status;
		}, format: function(date, i){
			var date = new Date(date.getTime() + (i || 0) * 86400 * 1000);
			return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
		}, appId: function(callback){
			callback = callback || angular.noop;
			if($rootScope.apiLoaded){
				callback();
				return;
			}
			$http.get('/weixin/?url=' + encodeURIComponent(location.href.substring(0, location.href.indexOf('#')))).then(function(result){
				$rootScope.apiLoaded = true;
				if(result.status == 200){
					wx.config({
						debug: false,
						appId: result.data.appId,
						timestamp: result.data.timestamp,
						nonceStr: result.data.nonceStr,
						url: location.href,
						signature: result.data.signature,
						jsApiList: ['onMenuShareAppMessage','onMenuShareWeibo','onMenuShareQQ','onMenuShareTimeline','scanQRCode']
					});
					callback();
				}else{
					$rootScope.consoleMsg = '加载微信appId失败：' + result.status;
				}
			});
		}
	}
});